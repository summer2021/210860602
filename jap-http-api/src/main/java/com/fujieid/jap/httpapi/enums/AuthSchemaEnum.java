package com.fujieid.jap.httpapi.enums;

public enum AuthSchemaEnum {
    basic,
    digest,
    bearer
}
