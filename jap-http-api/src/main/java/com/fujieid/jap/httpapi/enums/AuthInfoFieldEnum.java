package com.fujieid.jap.httpapi.enums;

public enum AuthInfoFieldEnum {
    header,
    params,
    body
}
