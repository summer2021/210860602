package com.fujieid.jap.httpapi.enums;

public enum HttpMethodEnum {
    post,
    get
}
