package com.fujieid.jap.httpapi.enums;

public enum ForBearerTokenEnum {
    by_header,
    by_params,
    by_body,
    by_basic,
    by_digest
}
