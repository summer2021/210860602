package com.test;

import com.fujieid.jap.core.JapUser;
import com.fujieid.jap.core.JapUserService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author zhihai.yu (mvbbb(a)foxmail.com)
 * @version 1.0.0
 * @since 1.0.0
 */
public class JapHttpApiUserServiceImpl implements JapUserService {
    /**
     * 模拟 DB 操作
     */
    private static final List<JapUser> userDatas = new ArrayList<>();

    static {
        for (int i = 0; i < 10 ; i++) {
            userDatas.add(new JapUser().setUserId(String.valueOf(i)).setUsername(String.valueOf(i)));
        }
        userDatas.add(new JapUser().setUsername("root").setUserId("123"));
    }

    @Override
    public JapUser getById(String userId) {
        return userDatas.stream().filter((user)->user.getUserId().equals(user)).findFirst().orElse(null);
    }

    @Override
    public JapUser getByName(String username) {
        return userDatas.stream().filter((user)->user.getUsername().equals(username)).findFirst().orElse(null);
    }

    @Override
    public boolean validPassword(String password, JapUser user) {
        return false;
    }

    /**
     * 根据第三方平台标识（platform）和第三方平台的用户 uid 查询数据库
     *
     * @param platform 第三方平台标识
     * @param uid      第三方平台的用户 uid
     * @return JapUser
     */
    @Override
    public JapUser getByPlatformAndUid(String platform, String uid) {
        return null;
    }

    /**
     * 创建并获取第三方用户，相当于第三方登录成功后，将授权关系保存到数据库（开发者业务系统中 social user -> sys user 的绑定关系）
     *
     * @param userInfo JustAuth 中的 AuthUser
     * @return JapUser
     */
    @Override
    public JapUser createAndGetSocialUser(Object userInfo) {
        return null;
    }

    /**
     * 保存 oauth 登陆的用户信息
     * @param platform  oauth2 平台名称
     * @param userInfo  Oauth 平台返回的基础用户信息
     * @param tokenInfo Oauth 平台返回的 token，开发者可以存储在数据库中
     * @return
     */
    @Override
    public JapUser createAndGetOauth2User(String platform, Map<String, Object> userInfo, Object tokenInfo) {
        return null;
    }

    @Override
    public JapUser createAndGetHttpApiUser(JapUser japUser) {
        userDatas.add(japUser);
        return userDatas.get(userDatas.size()-1);
    }
}
