package com.test;

import com.fujieid.jap.core.cache.JapLocalCache;
import com.fujieid.jap.core.config.JapConfig;
import com.fujieid.jap.core.result.JapResponse;
import com.fujieid.jap.httpapi.HttpApiConfig;
import com.fujieid.jap.httpapi.HttpApiStrategy;
import com.fujieid.jap.httpapi.enums.AuthInfoFieldEnum;
import com.fujieid.jap.httpapi.enums.AuthSchemaEnum;
import com.fujieid.jap.httpapi.enums.HttpMethodEnum;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

/**
 * @author zhihai.yu (mvbbb(a)foxmail.com)
 * @version 1.0.0
 * @since 1.0.0
 */
@RestController
public class MyController {

    @GetMapping("/basic")
    public ResponseEntity authBasic(HttpServletRequest request, HttpServletResponse response ){
        HttpApiStrategy httpApiStrategy = new HttpApiStrategy(new JapHttpApiUserServiceImpl(), new JapConfig(),new JapLocalCache());
        HttpApiConfig httpApiConfig = new HttpApiConfig()
                .setAuthSchema(AuthSchemaEnum.basic)
                .setHttpMethod(HttpMethodEnum.get)
                .setAuthInfoField(AuthInfoFieldEnum.body)
                .setLoginUrl("localhost:8088/api/v1/source1");

        JapResponse authenticate = httpApiStrategy.authenticate(httpApiConfig, request, response);
        if(authenticate.isSuccess()){
            return new ResponseEntity(200,"login success",authenticate.getData());
        }else{
            return new ResponseEntity(403,"login failure",authenticate.getData());
        }
    }

    @GetMapping("/digest")
    public ResponseEntity authDigest(HttpServletRequest request,HttpServletResponse response){
        HttpApiStrategy httpApiStrategy = new HttpApiStrategy(new JapHttpApiUserServiceImpl(), new JapConfig(),new JapLocalCache());
        // 开发者自定义请求头
        HashMap<String, String> customHeader = new HashMap<>();
        customHeader.put("User-Agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36 Edg/92.0.902.55");
        HttpApiConfig httpApiConfig = new HttpApiConfig()
                .setAuthSchema(AuthSchemaEnum.digest)
                .setHttpMethod(HttpMethodEnum.get)
                .setLoginUrl("http://localhost:8088/api/v1/source1")
                // 指定前端传入的用户信息的是以什么样的方式传递进来的 param，header，body
                .setAuthInfoField(AuthInfoFieldEnum.params)
                .setCustomHeaders(customHeader);


        JapResponse authenticate = httpApiStrategy.authenticate(httpApiConfig, request, response);
        if(authenticate.isSuccess()){
            return new ResponseEntity(200,"Login success",authenticate.getData());
        }else{
            return new ResponseEntity(403,"Login failure",authenticate.getData());
        }
    }

    @GetMapping("/bearer")
    public ResponseEntity authBearer(HttpServletRequest request,HttpServletResponse response){
        HttpApiStrategy httpApiStrategy = new HttpApiStrategy(new JapHttpApiUserServiceImpl(), new JapConfig(),new JapLocalCache());
        HttpApiConfig httpApiConfig = new HttpApiConfig()
                .setAuthSchema(AuthSchemaEnum.bearer)
                .setHttpMethod(HttpMethodEnum.get)
                .setLoginUrl("http://localhost:8088/api/v2/source1")
                .setAuthInfoField(AuthInfoFieldEnum.params);


        JapResponse authenticate = httpApiStrategy.authenticate(httpApiConfig, request, response);
        if(authenticate.isSuccess()){
            return new ResponseEntity(200,"Login success",authenticate.getData());
        }else{
            return new ResponseEntity(403,"Login failure",authenticate.getData());
        }
    }

    @GetMapping("/auth/bearer")
    public ResponseEntity authBearer1(HttpServletRequest request,HttpServletResponse response){
        HttpApiStrategy httpApiStrategy = new HttpApiStrategy(new JapHttpApiUserServiceImpl(), new JapConfig(),new JapLocalCache());
        HashMap<String, String> customHeader = new HashMap<>();
        customHeader.put("User-Agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36 Edg/92.0.902.55");
        HttpApiConfig httpApiConfig = new HttpApiConfig()
            .setAuthSchema(AuthSchemaEnum.bearer)
            .setHttpMethod(HttpMethodEnum.get)
            .setLoginUrl("http://localhost:8088/auth/token")
            .setAuthInfoField(AuthInfoFieldEnum.params)
            .setCustomHeaders(customHeader);

        JapResponse authenticate = httpApiStrategy.authenticate(httpApiConfig, request, response);
        if(authenticate.isSuccess()){
            return new ResponseEntity(200,"Login success",authenticate.getData());
        }else{
            return new ResponseEntity(403,"Login failure",authenticate.getData());
        }
    }

    @GetMapping("/token")
    public ResponseEntity getToken(HttpServletRequest request,HttpServletResponse response){
        HttpApiStrategy httpApiStrategy = new HttpApiStrategy(new JapHttpApiUserServiceImpl(), new JapConfig(),new JapLocalCache());
        HttpApiConfig httpApiConfig = new HttpApiConfig()
            .setAuthSchema(AuthSchemaEnum.basic)
            .setHttpMethod(HttpMethodEnum.get)
            .setLoginUrl("http://localhost:8088/auth/token")
            .setAuthInfoField(AuthInfoFieldEnum.params);
        JapResponse authenticate = httpApiStrategy.authenticate(httpApiConfig, request, response);
        if(authenticate.isSuccess()){
            return new ResponseEntity(200,authenticate.getMessage(),authenticate.getData());
        }else{
            return new ResponseEntity(403,authenticate.getMessage(),authenticate.getData());
        }
    }
}
